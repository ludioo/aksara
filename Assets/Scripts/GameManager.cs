﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }
    
    public void About()
    {
        SceneManager.LoadScene("About");
    }

    public void Home()
    {
        SceneManager.LoadScene("Menu");
    }

    private void Update()
    {
        
    }
}
