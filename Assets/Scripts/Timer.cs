﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timeStart = 100;
    public Text timer_txt;
    public Text resultTimer_txt;
    bool timerActive = false;
    public GameObject failPanel;
    public AnswerChecker answerChecker;
    public GameObject backgroundItems;
    public Text failSoal_txt;

    // Start is called before the first frame update
    void Start()
    {
        timerActive = true;
        resultTimer_txt.text = timeStart.ToString("0");
        timer_txt.text = timeStart.ToString("0");
        failSoal_txt.text = answerChecker.indexSoal.ToString("0");
    }

    // Update is called once per frame
    void Update()
    {
        if(timerActive)
        {
            timeStart -= Time.deltaTime;
            resultTimer_txt.text = timeStart.ToString("0");
            timer_txt.text = timeStart.ToString("0");
            failSoal_txt.text = answerChecker.indexSoal.ToString("0");
        }

        if(timeStart <= 0)
        {
            answerChecker.soal[answerChecker.indexSoal].SetActive(false);
            failPanel.SetActive(true);
            timeStart = 0;
            timerStop();
            backgroundItems.SetActive(false);
        }

    }

    public void timerStop()
    {
        timerActive = !timerActive;
    }
}
