﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AnswerChecker : MonoBehaviour
{
    public GameManager gm;
    public GameObject[] soal;
    public int indexSoal = 1;
    public int score = 0;
    public Text score_txt;
    public Text resultScore_txt;
    public Animator scorePlus;
    public GameObject scorePlus_obj;
    public AudioClip correct;
    public AudioClip wrong;
    public AudioSource audioSource;
    public Timer timer;
    public GameObject resultPanel;
    public Animator resultPanelAnim;
    public Text failScore_txt;
    public GameObject backgroundItems;
    // Start is called before the first frame update
    void Start()
    {
        score_txt.text = score.ToString("0");
        resultScore_txt.text = score.ToString("0");
        failScore_txt.text = score.ToString("0");
    }

    // Update is called once per frame
    void Update()
    {
        resultScore_txt.text = score.ToString("0");
        score_txt.text = score.ToString("0");
        failScore_txt.text = score.ToString("0");
        
    }
    
    public void CheckJawaban(bool answer)
    {
        if(answer == true)
        {
            score += 10;
            soal[indexSoal-1].SetActive(false);
            soal[indexSoal].SetActive(true);
            scorePlus.Play("scoreplus");
            audioSource.PlayOneShot(correct);
            indexSoal++;
            Debug.Log(indexSoal);

        }
        else
        {
            audioSource.PlayOneShot(wrong);
            soal[indexSoal-1].SetActive(false);
            soal[indexSoal].SetActive(true);
            indexSoal++;
            Debug.Log(indexSoal);
        }

        if(indexSoal==11)
        {
            soal[indexSoal-1].SetActive(false);
            resultPanel.SetActive(true);
            resultPanelAnim.Play("resultPanel");
            backgroundItems.SetActive(false);
            timer.timerStop();
        }
    }
}
